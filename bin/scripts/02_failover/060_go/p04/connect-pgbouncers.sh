#!/usr/bin/env bash

set -euo pipefail
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
UNSYMLINKED_SCRIPT_DIR="$(readlink -f "${SCRIPT_DIR}" || readlink "${SCRIPT_DIR}" || echo "${SCRIPT_DIR}")"
# shellcheck disable=SC1091,SC1090
source "${UNSYMLINKED_SCRIPT_DIR}/../../../../source_vars"

if [[ -z $PGBOUNCERS_GCP ]]; then
  echo "You must set PGBOUNCERS_GCP in source_vars"
fi

for pgbouncer in ${PGBOUNCERS_GCP[*]}
do
    echo "Connecting to $pgbouncer"
    echo "After logging in run:"
    echo "    sudo gitlab-ctl pgb-console"
    echo "    show databases;"
    echo "    show servers;"
    echo "Confirm the new primary"
    ssh "$pgbouncer"
done
