#!/usr/bin/env bash

set -euo pipefail
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
UNSYMLINKED_SCRIPT_DIR="$(readlink -f "${SCRIPT_DIR}" || readlink "${SCRIPT_DIR}" || echo "${SCRIPT_DIR}")"
# shellcheck disable=SC1091,SC1090
source "${UNSYMLINKED_SCRIPT_DIR}/../../../../workflow-script-commons.sh"

if [[ -z $POSTGRESQL_GCP_WAL_SECONDARY ]]; then
  echo "You must set POSTGRESQL_GCP_WAL_SECONDARY in source_vars"
fi

last_event=$(ssh "$POSTGRESQL_GCP_WAL_SECONDARY" 'sudo gitlab-psql -t -d gitlabhq_production -c "SELECT now() - pg_last_xact_replay_timestamp();"')

echo "Last event seen for $POSTGRESQL_GCP_WAL_SECONDARY was${last_event} ago."

echo "-----"
for secondary in ${POSTGRESQL_GCP_SECONDARIES[*]}
do
    last_event=$(ssh "$secondary" 'sudo gitlab-psql -t -d gitlabhq_production -c "SELECT now() - pg_last_xact_replay_timestamp();"')
    echo "Last event seen for $secondary was${last_event} ago."
done
