# Resolve repo & wiki sync failures

## First and foremost

*Don't Panic*

## Diagnose errors

- Visit [Kibana](https://log.gitlab.net/app/kibana)
- Select `pubsub-rails-inf-gprd`
- Search for `"Error syncing repository"`

### Export failures to CSV

```ruby
# Repositories that failed to sync
repo_failures = Geo::ProjectRegistry.failed_repos

data = CSV.open('/tmp/repo_sync_failures.csv', 'w')
repo_failures.each do |fail|
   data << [fail.project_id, fail.last_repository_sync_failure&.gsub("\n", " ")]
end; nil
data.close
```

```ruby
# Wikis that failed to sync
wiki_failures = Geo::ProjectRegistry.failed_wikis

data = CSV.open('/tmp/wiki_sync_failures.csv', 'w')
wiki_failures.each do |fail|
   data << [fail.project_id, fail.last_wiki_sync_failure&.gsub("\n", " ")]
end; nil
data.close
```

### Get unsynced project count

```ruby
finder = Geo::ProjectRegistryFinder.new
finder.send(:fdw_find_unsynced_projects).count
```

### Get unsynced wiki count

```ruby
finder = Geo::ProjectRegistryFinder.new
synced_wikis = finder.send(:fdw_find_synced_wikis).pluck(:project_id); nil
wikis = Project.with_wiki_enabled.pluck(:id); nil
diff = wikis - synced_wikis
diff.count
```

Extract the failures:

```ruby
finder.count_failed_wikis

Geo::ProjectRegistry.where(project_id: diff).where('wiki_retry_count > 0').count
```

## Force resync

When you have a list of projects you'd like to resync:

```ruby
# Repository
registries_to_resync.update_all(repository_retry_at: Time.now, resync_repository: true)

# Wiki
registries_to_resync.update_all(wiki_retry_at: Time.now, resync_wiki: true)
```

### Forcing current process to fetch

The lines above will only mark the repositories as dirty so that a Sidekiq
worker can be scheduled to resync. If you want the current process to help
with the updates, you can run:

```ruby
Geo::ProjectRegistry.dirty.find_each do |registry|
    Geo::ProjectSyncWorker.new.perform(registry.project_id, Time.now)
end
```

## Force a resync using snapshot

Geo defaults to a `git fetch` approach to fetch changes from the primary. If
this fails repeatedly, it will switch to a snapshot-based approach. You can
force some registries to switch to snapshotting with the following:

```ruby
# Ensure the feature is enabled
Feature.enable(:geo_redownload_with_snapshot)

# Repositories
registries_to_resync.update_all(repository_retry_at: Time.now, resync_repository: true, force_to_redownload_repository: true)

# Wikis
registries_to_resync.update_all(wiki_retry_at: Time.now, resync_wiki: true, force_to_redownload_wiki: true)
```
